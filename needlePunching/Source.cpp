#include <iostream>
#include <cstdio>
#include <vector>
#include <string>
#include <cmath>
#include <ctime>
#include <random>

using namespace std;

//	Constants
	const double eps = 1e-5;
	const double Pi = 3.1415926535;
	const double needleRadius = 0.0004;
	const double needleHeight = 0.0005;
//

//	Variables
	double threadLength = 0.005;
	double finitElSize = threadLength / 5;
	int firstMeshCountX = 5;
	double firstMeshStepX = 0.002;
	int firstMeshCountY = 5;
	double firstMeshStepY = 0.002;
	double firstMeshIndentX = needleRadius;
	double firstMeshIndentY = needleRadius;
	int secondMeshCountX = 0;
	double secondMeshStepX = 0.002;
	int secondMeshCountY = 5;
	double secondMeshStepY = 0.002;
	double secondMeshIndentX = needleRadius + 0.001;
	double secondMeshIndentY = needleRadius + 0.001;
	int N = 1000;

	double maxX = 0.01;
	double maxY = 0.01;
	double maxZ = 0.0035;
//

//	Random numbers generators
	mt19937 gen(time(NULL));
	uniform_real_distribution<> urdX;
	uniform_real_distribution<> urdY;
	uniform_real_distribution<> urdZ;
//

double sqr(double x) {
	return x * x;
}

double ABS(double x) {
	return (x > 0 ? x : -x);
}

class Point {
public:
	double x, y, z;

	Point() {
		x = y = z = 0;
	}
	
	Point(double _x, double _y, double _z) : x(_x), y(_y), z(_z) {};

	double dist(Point A) {
		return sqrt(sqr(A.x - x) + sqr(A.y - y));
	}

	double dist3d(Point A) {
		return sqrt(sqr(A.x - x) + sqr(A.y - y) + sqr(A.z - z));
	}

	bool operator == (Point right) {
		return (x == right.x && y == right.y && z == right.z);
	}

	bool operator != (Point right) {
		return !(*this == right);
	}
};

class Line {
public:
	Point leftTop, leftBottom, rightTop, rightBottom;
	double A, B, C;

	Line(Point I, Point II) {
		leftTop = I;
		rightTop = II;
		A = II.y - I.y;
		B = I.x - II.x;
		C = II.x * I.y - I.x * II.y;
	}

	Line() {
		leftTop = Point(0, 0, 0);
		leftBottom = Point(0, 0, 0);
		rightTop = Point(0, 0, 0);
		rightBottom = Point(0, 0, 0);
	}

	bool isIntersect(Point O) {
		Point I = leftTop;
		Point II = rightTop;

		double distPtoL = sqr(A * O.x + B * O.y + C) / (sqr(A) + sqr(B));
		if (sqr(needleRadius) < distPtoL) {
			return false;
		}
		return true;
	}

	static Line generate() {
		double x = urdX(gen);
		double y = urdY(gen);
		double z = urdZ(gen);

		uniform_real_distribution<> urdAngleX(-Pi, Pi);
		double angleX = urdAngleX(gen);

		double newX = threadLength * cos(angleX) + x;
		double newY = threadLength * sin(angleX) + y;
		double newZ = urdZ(gen);
		

		if (newX > maxX) {
			double k = (maxX - x) / (newX - x);
			newX = maxX;
			newY = k * newY + y - k * y;
			newZ = k * newZ + z - k * z;
		}

		if (newY > maxY) {
			double k = (maxY - y) / (newY - y);
			newX = k * newX + x - k * x;
			newY = maxY;
			newZ = k * newZ + z - k * z;
		}
		if (newX < 0) {
			double k = newX / (newX - x);
			newX = 0;
			newY = newY + k * y - k * newY;
			newZ = newZ + k * z - k * newZ;
		}

		if (newY < 0) {
			double k = newY / (newY - x);
			newX = newX + k * x - k * newX;
			newY = 0;
			newZ = newZ + k * z - k * newZ;
		}


		Point I(x, y, z);
		Point II(newX, newY, newZ);
		
		return Line(I, II);
	}
};

const Point nPoint(0, 0, 0);

void readVars() {
	double tmp = 0;
	printf("Number of threads (%d)\n", N);
	cin >> tmp;
	if (tmp != 0) {
		N = tmp;
		tmp = 0;
	}
	printf("Thread length (%.5f)\n", threadLength);
	cin >> tmp;
	if (tmp != 0) {
		threadLength = tmp;
		tmp = 0;
	}
	printf("Max X (%.5f)\n", maxX);
	cin >> tmp;
	if (tmp != 0) {
		maxX = tmp;
		tmp = 0;
	}
	printf("Max Y (%.5f)\n", maxY);
	cin >> tmp;
	if (tmp != 0) {
		maxY = tmp;
		tmp = 0;
	}
	printf("Maximum length of finite element (%.5f)\n", finitElSize);
	cin >> tmp;
	if (tmp != 0) {
		finitElSize = tmp;
		tmp = 0;
	}
	printf("First mesh amount of needles per line (%d)\n", firstMeshCountX);
	cin >> tmp;
	if (tmp != 0) {
		firstMeshCountX = tmp;
		tmp = 0;
	}
	printf("First mesh distance between needles in line (%.5f)\n", firstMeshStepX);
	cin >> tmp;
	if (tmp != 0) {
		firstMeshStepX = tmp;
		tmp = 0;
	}
	printf("First mesh amount of needles per column (%d)\n", firstMeshCountY);
	cin >> tmp;
	if (tmp != 0) {
		firstMeshCountY = tmp;
		tmp = 0;
	}
	printf("First mesh distance between needles in column (%.5f)\n", firstMeshStepY);
	cin >> tmp;
	if (tmp != 0) {
		firstMeshStepY = tmp;
		tmp = 0;
	}
	printf("Second mesh amount of needles per line (%d)\n", secondMeshCountX);
	cin >> tmp;
	if (tmp != 0) {
		secondMeshCountX = tmp;
		tmp = 0;
	}
	if (secondMeshCountX != 0) {
		printf("Second mesh distance between needles in line (%.5f)\n", secondMeshStepX);
		cin >> tmp;
		if (tmp != 0) {
			secondMeshStepX = tmp;
			tmp = 0;
		}

		printf("Second mesh indent X (%.5f)\n", secondMeshIndentX);
		cin >> tmp;
		if (tmp != 0) {
			secondMeshIndentX = tmp;
			tmp = 0;
		}
		printf("Second mesh amount of needles per column (%d)\n", secondMeshCountY);
		cin >> tmp;
		if (tmp != 0) {
			secondMeshCountY = tmp;
			tmp = 0;
		}
		if (secondMeshCountY != 0) {
			printf("Second mesh distance between needles in column (%.5f)\n", secondMeshStepY);
			cin >> tmp;
			if (tmp != 0) {
				secondMeshStepY = tmp;
				tmp = 0;
			}
			printf("Second mesh indent Y (%.5f)\n", secondMeshIndentY);
			cin >> tmp;
			if (tmp != 0) {
				secondMeshIndentY = tmp;
				tmp = 0;
			}
		}
	}
	urdX = uniform_real_distribution<>(0, maxX);
	urdY = uniform_real_distribution<>(0, maxY);
	urdZ = uniform_real_distribution<>(0, maxZ);
}

vector<Point> makeFinite(Point A, Point B) {
	vector<Point> tmp;
	if (A == nPoint) {
		tmp.push_back(B);
		return tmp;
	}
	if (B == nPoint) {
		tmp.push_back(A);
		return tmp;
	}
	tmp.push_back(A);
	double length = A.dist3d(B);
	double amount = length / finitElSize;
	if (amount > 1) {
		int elCount = ceil(amount);
		for (int i = 1; i < elCount; i++) {
			double k = (1. * i) / elCount;
			double newX = (A.x * (1 - k) + B.x * k);
			double newY = (A.y * (1 - k) + B.y * k);
			double newZ = (A.z * (1 - k) + B.z * k);
			tmp.push_back(Point(newX, newY, newZ));
		}
	}
	tmp.push_back(B);
	return tmp;
}

int main() {
	
	readVars();
	
	int startTime = clock();

	srand(time(NULL));
	vector<Line> lines(N);
	for (int i = 0; i < N; i++) {
		lines[i] = Line::generate();
	}
	
	int pointsCnt = firstMeshCountX * firstMeshCountY + secondMeshCountX * secondMeshCountY;
	vector<Point> points(pointsCnt);
	int counter = 0;
	double posX = firstMeshIndentX;
	for (int i = 0; i < firstMeshCountX; i++) {
		double posY = firstMeshIndentY;
		for (int j = 0; j < firstMeshCountY; j++) {
			points[counter++] = Point(posX, posY, 0);
			posY += firstMeshStepY;
		}
		posX += firstMeshStepX;
	}
	posX = secondMeshIndentX;
	for (int i = 0; i < secondMeshCountX; i++) {
		double posY = secondMeshIndentY;
		for (int j = 0; j < secondMeshCountY; j++) {
			points[counter++] = Point(posX, posY, 0);
			posY += secondMeshStepY;
		}
		posX += secondMeshStepX;
	}

	for (int j = 0; j < pointsCnt; j++) {
		vector<Line> newLines;
		Point O = points[j];
		for (int i = 0; i < lines.size(); i++) {
			Line l = lines[i];
			Point I = l.leftTop;
			Point II = l.rightTop;
			double A = l.A;
			double B = l.B;
			double C = l.C;
			double x = O.x;
			double y = O.y;
			if (l.isIntersect(O)) {
				double C1 = A * y - B * x;
				double xH = (-B * C1 - A * C) / (sqr(A) + sqr(B));
				double yH = (A * C1 - B * C) / (sqr(A) + sqr(B));
				Point H(xH, yH, 0);
				double OH = O.dist(H);
				double IH = I.dist(H);
				double IIH = II.dist(H);
				double ItoII = I.dist(II);
				double KH = sqrt(ABS(sqr(needleRadius) - sqr(OH)));
				double IO = O.dist(I);
				double IIO = O.dist(II);

				if (ABS(IH + IIH - ItoII) < eps) {
					double k1 = (IH - KH) / ItoII;
					double newX = k1 * (II.x - I.x) + I.x;
					double newY = k1 * (II.y - I.y) + I.y;
					double newZ = k1 * (II.z - I.z) + I.z;
					Line l1(I, Point(newX, newY, newZ));
					l1.rightBottom = Point(newX, newY, needleHeight + maxZ);
					l1.leftBottom = l.leftBottom;
					if (IO > needleRadius + eps) {
						if (newX > 0 && newX < maxX && newY < maxY && newY > 0 && newZ < maxZ && newZ > 0) {
							newLines.push_back(l1);
						}
					}
					double k2 = (IIH - KH) / ItoII;
					newX = k2 * (I.x - II.x) + II.x;
					newY = k2 * (I.y - II.y) + II.y;
					newZ = k2 * (I.z - II.z) + II.z;
					Line l2(Point(newX, newY, newZ), II);
					l2.leftBottom = Point(newX, newY, needleHeight + maxZ);
					l2.rightBottom = l.rightBottom;	
					if (IIO > needleRadius + eps) {
						if (newX > 0 && newX < maxX && newY < maxY && newY > 0 && newZ < maxZ && newZ > 0) {
							newLines.push_back(l2);
						}
					}
					continue;
				} else {
					if (IH < KH) {
						double k = (KH - IH) / ItoII;
						double newX = I.x - k * I.x + k * II.x;
						double newY = I.y - k * I.y + k * II.y;
						double newZ = I.z - k * I.z + k * II.z;
						Line l1(Point(newX, newY, newZ), II);
						
						l1.leftBottom = Point(newX, newY, needleHeight + maxZ);
						newLines.push_back(l1);
						
						continue;
					} else
					if (IIH < KH) {
						double k = (KH - IIH) / ItoII;
						double newX = II.x - k * II.x + k * I.x;
						double newY = II.y - k * II.y + k * I.y;
						double newZ = II.z - k * II.z + k * I.z;
						Line l1(I, Point(newX, newY, newZ));
						
						l1.rightBottom = Point(newX, newY, needleHeight + maxZ);
						newLines.push_back(l1);
						continue;
					}
				}
			}
			newLines.push_back(l);
		}
		swap(newLines, lines);
	}
	FILE* out = fopen("output.k", "w");
	fprintf(out, "*KEYWORD\n*NODE\n");
	for (int i = 0, points = 1; i < lines.size(); i++) {
		Point A = lines[i].leftBottom;
		Point B = lines[i].leftTop;
		Point C = lines[i].rightTop;
		Point D = lines[i].rightBottom;

		vector<Point> AB = makeFinite(A, B);
		vector<Point> BC = makeFinite(B, C);
		vector<Point> CD = makeFinite(C, D);
		for (int i = 0; i < AB.size(); i++) {
			fprintf(out, "%8d %.8E  %.8E  %.8E%8d%8d\n", points++, AB[i].x, AB[i].y, AB[i].z, 0, 0);
		}
		for (int i = 1; i < BC.size(); i++) {
			fprintf(out, "%8d %.8E  %.8E  %.8E%8d%8d\n", points++, BC[i].x, BC[i].y, BC[i].z, 0, 0);
		}
		for (int i = 1; i < CD.size(); i++) {
			fprintf(out, "%8d %.8E  %.8E  %.8E%8d%8d\n", points++, CD[i].x, CD[i].y, CD[i].z, 0, 0);
		}
	}
	int numPoints = 1;
	int i = 0;
	int number = 1;
	fprintf(out, "*ELEMENT_BEAM\n");
	while (i != lines.size()) {
		Point A = lines[i].leftBottom;
		Point B = lines[i].leftTop;
		Point C = lines[i].rightTop;
		Point D = lines[i].rightBottom;
		
		if (i != 0) {
			if (lines[i - 1].rightTop.dist(lines[i].leftTop) < 2 * needleRadius + eps) {
				fprintf(out, "%8d%8d%8d%8d%8d\n", number++, 1, numPoints, numPoints - 1, 0);
			}
		}

		int cnt = 0;
		if (A != nPoint) {
			cnt = ceil(A.dist3d(B) / finitElSize);
		}
		for (int i = 0; i < cnt; i++) {
			fprintf(out, "%8d%8d%8d%8d%8d\n", number++, 1, numPoints++, numPoints + 1, 0);
		}
		cnt = ceil(B.dist3d(C) / finitElSize);
		for (int i = 0; i < cnt; i++) {
			fprintf(out, "%8d%8d%8d%8d%8d\n", number++, 1, numPoints++, numPoints + 1, 0);
		}
		cnt = 0;
		if (D != nPoint) {
			cnt = ceil(C.dist3d(D) / finitElSize);
		}
		for (int i = 0; i < cnt; i++) {
			fprintf(out, "%8d%8d%8d%8d%8d\n", number++, 1, numPoints++, numPoints + 1, 0);
		}
		numPoints++;
		i++;
	}
	fprintf(out, "*END");
	FILE* Time = fopen("time.txt", "w");
	fprintf(Time, "%d - Working time\n", -startTime + clock());
	return 0;
}